import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Vuetify from '/src/plugins/vuetify.js'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter)
//router
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false


const router = new VueRouter({
  routes: [
    // dynamic segments start with a colon
    { path: '/', component: App },
    { path: '/company/:company_slug/category/:category_slug', name:"search", component: App },
    
  ],
  mode: "history"
})
new Vue({
  router,
  vuetify:Vuetify,
  render: h => h(App),
}).$mount('#app')
